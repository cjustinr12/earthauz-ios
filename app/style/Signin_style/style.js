import React from 'react';
import { StyleSheet } from "react-native"
import {variable} from '../Variable/variable'

export default StyleSheet.create({
    /* Sing IN */
    sign_in_container: {
        // backgroundColor: "#ffff",
        marginLeft: 20,
        marginRight: 20
    },

    sign_in_logo: {
        marginTop: 40,
        marginBottom: 20,
        width: 200,
        height: 150,
        alignSelf: "center"
    },

    error_txt: {
        fontSize: 12,
        paddingTop: 1,
        paddingBottom: 3,
        color: variable._error_text
    },

    checkbox_container: {
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 12,
        marginTop: 12,
        flexDirection: 'row'
    },

    checkbox_sign_in : {
        marginRight: 10,
    },

    checkbox_text: {
        fontSize: 15,
        flex: 1, 
        flexWrap: 'wrap'
    },

    checkbox_text_modal: {
        color: variable._eh_blue
    }

});