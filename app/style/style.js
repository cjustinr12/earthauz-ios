import React from 'react';
import { StyleSheet } from "react-native"
import {variable} from './Variable/variable'

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignContent: "center",
    },

    container_center: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

    white_bg: {
        backgroundColor: "#ffff",
    },

    input: {
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomColor: "gray",
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 8
    },

    button_disable: {
        backgroundColor: variable._eh_blue_disable
    },

    button_enable: {
        backgroundColor: variable._eh_blue
    },

    button_custom: {
        padding: 15,
        height: 55,
        borderRadius: 8,
        borderColor: variable._eh_blue,
        /* backgroundColor: variable._eh_blue, */
        borderWidth: StyleSheet.hairlineWidth,
    },
    btn_txt: {
        fontSize: 16,
        color: variable._eh_white,
        textAlign: "center"
    }
});