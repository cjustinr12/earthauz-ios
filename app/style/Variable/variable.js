import React, { Component } from 'react';
import { Text, View } from 'react-native';

/* export default class  extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
        <View>
        </View>
        )
    }
} */

/* export const {_eh_blue}="#0557cd";
export const {_eh_white}="#FFFFFF"; */

export const variable = {
    _eh_blue: "#0557cd",
    _eh_white: "#FFFFFF",
    _eh_blue_disable: "#8db6f0",
    _error_text: "#FA6644",
}