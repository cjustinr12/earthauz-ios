import React from 'react';
import { StyleSheet } from "react-native"
import {variable} from '../Variable/variable'

export default StyleSheet.create({
    startup_logo: {
        width: 200,
        height: 200,
        alignSelf: "center"
    },
    startup_container: {
        flex: 1,
        justifyContent: "center",
        alignContent: "center",
        marginLeft: 50,
        marginRight: 50
    },
    progressbar: {
        height: 5,
        backgroundColor: "#ededed",
        borderRadius:10,
    },
    progress: {
        position: "absolute",
        top: 0,
        left: 0,
        borderRadius:10,
    },
    red: {
        color: 'red'
    },
});

/* <color name="ehColorBlue">#0557cd</color>
<color name="pageBackground">#f9f8f8</color>
<color name="contentTextColor">#8d8d8d</color>
<color name="switch_checked_color">#f8b118</color>
<color name="switch_default_color">#eeeeee</color>
<color name="search_history_header">#626262</color>
<color name="border_bottom_color">#c4c4c4</color>
<color name="search_active_button_color">#e8e9ea</color>
<color name="icon_active_color">#0d62ce</color>
<color name="black">#000000</color>
<color name="white">#FFFFFF</color>
<color name="progressBarBackground">#ededed</color>
<color name="progressBarTint">#a9a9a9</color>
<color name="yellowish">#f7b753</color>
<color name="inputTextColor">#c1c1c1</color>
<color name="search_background_color">#f5f5f5</color>
<color name="search_text_color">#919191</color> */