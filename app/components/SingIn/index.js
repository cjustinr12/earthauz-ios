import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, TextInput, TouchableWithoutFeedback, Keyboard, TouchableOpacity, Alert, Image, SafeAreaView, ScrollView } from 'react-native';
import {Picker} from '@react-native-community/picker';
import {Formik} from 'formik';
import CheckBox from 'react-native-check-box'
import * as RootNavigation from '../Navroot/index.js'
import style from '../../style/Signin_style/style'
import g_style from '../../style/style'
import * as yup from 'yup';
/* import Icon from 'react-native-vector-icons/FontAwesome'; */

const registrationSchema = yup.object({
    registration_type_id: yup.string()
        .required('Account Type is a required field'), 
    firstname: yup.string()
        .required('Name is a required field'), 
    email: yup.string()
        .email()
        .required('Email Address is a required field'), 
    password: yup.string()
        .min(8)
        .required('Password is a required field'), 
    confirm_password: yup.string()
        .test('passwords-match', 'Password Mismatch', function(value) {
            return this.parent.password === value;
        })
        .required('Confirm Password is a required field')
})

export default class  extends Component {
    constructor(props) {
        super();
        this.state = {
            buttonEnabled: true,
            isChecked: false,
            account_type: '',
        };
        this.register = this.register.bind(this);
        this.alert_ = this.alert_.bind(this);
        /* this.hadleChangeText = this.hadleChangeText.bind(this); */
    }

    

    
    UNSAFE_componentWillMount(){
        fetch('http://v2.staging.earthhauz.com/en/api/v1/user/get_registration_type', {
            method: 'POST',
            // withCredentials: true,
            // credentials: 'include',
            headers: {
                'api_key': '81c5ef141a34911c353f859c8af32ea0686b99b867f7641c769644b19047a50e',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                
            },
            /* credentials: "include", */
            /* body: JSON.stringify({
                key: '81c5ef141a34911c353f859c8af32ea0686b99b867f7641c769644b19047a50e',
            }) */
        })
        .then((response) => response.json())
        .then((res) => {
            // this.setState({account_type:res.result});
            // console.log(this.state.account_type.length);
            var account_type = [];
            account_type.push(<Picker.Item key={"0"} value={""} label={"Account Type"}/>);
            for (let i = 0; i < res.result.length; i++) {
                var val = res.result;
                
                account_type.push(<Picker.Item key={i} value={val[i].id} label={val[i].type} />);
                
            }
            this.setState({account_type_list:account_type});
        })
        .done();
    }

    /* hadleChangeText() {
        console.log();
    } */
    /* test = () => {
        console.log('click');
        if (this.state.isChecked) {
            this.setState({
                buttonEnabled:true
            })
        }else{
            this.setState({
                buttonEnabled:false
            })
        }
    } */

    register = (val) => {
        fetch('http://v2.staging.earthhauz.com/en/api/v1/react-native/User/register', {
            method: 'POST',
            headers: {
                'api_key': '81c5ef141a34911c353f859c8af32ea0686b99b867f7641c769644b19047a50e',
                /* 'Accept': 'application/json',
                'Content-Type': 'application/json', */
                'Accept': '*',
                'Content-Type': '*',
                
            },
            body: JSON.stringify({
                firstname: val.firstname,
                registration_type_id: val.registration_type_id,
                password: val.password,
                email: val.email
            })
        })
        .then((response) => response.json())
        /* .then(response => {
            console.log(JSON.stringify(response, null, 4))
            return response.json()
        }) */
        .then((res) => {
            /* console.log('doneeeeeeeeee');
            console.log(res); */
            console.log(res);
            var data = {};
            var stat;
            if (res.success == "success") {
                data = {title: "Successful!", 
                    msg: res.msg,
                    btn_txt: "OK",
                    redirect: true,
                    page_to_redirect: 'Login'
                };
                
                this.setState({
                    buttonEnabled: true,
                    isChecked: false,
                    account_type: ''
                });

                stat = true;
            }else{
                data = {title: "Successful!", 
                    msg: res.msg,
                    btn_txt: "OK",
                    redirect: false,
                    page_to_redirect: ''
                };
                stat = false;
            }
            this.alert_(data);
            return stat;
        })
        /*.catch((error) => {
            console.log(JSON.stringify(error));
        }) */
        .done();
    }

    alert_ = (data) => {
        if (data.redirect) {
            Alert.alert(data.title,data.msg, [
                {text: data.btn_txt,onPress: () => RootNavigation.navigate(data.page_to_redirect)}
            ])
        }else{
            Alert.alert(data.title,data.msg, [
                {text: data.btn_txt}
            ])
        }
        
    }

    register_TEST = (val) => {
        var data = {title: "Successful", 
                    msg: "Owright!",
                    btn_txt: "OK",
                    redirect: true,
                    page_to_redirect: 'Home'
                };
        this.alert_(data);
    }

    render() {
        
        return (
            <SafeAreaView>
            <ScrollView>
            <View style={g_style.container,style.sign_in_container}>
                <Formik 
                    initialValues={{ registration_type_id: '', firstname: '', email: '', password: '', confirm_password: '' }}
                    validationSchema={registrationSchema}
                    onSubmit={(Values, actions) => {
                        if (this.register(Values)) {
                            actions.resetForm();
                        }
                    }}>
                    {(props) => (
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <View>
                                <Image 
                                style={style.sign_in_logo}
                                source={ require('../../assets/img/EH-1-2.png')} />
                                <View style={g_style.input}>
                                    <Picker
                                        selectedValue={this.state.account_type}
                                        onValueChange={(itemValue, itemIndex) =>{
                                            props.setFieldValue('registration_type_id', itemValue)
                                            this.setState({account_type: itemValue})
                                        }}
                                    >
                                        {this.state.account_type_list}
                                    </Picker>
                                </View>
                                <Text style={style.error_txt}>{props.touched.registration_type_id && props.errors.registration_type_id}</Text>
                                <TextInput 
                                    style={g_style.input}
                                    placeholder={"firstname"}
                                    defaultValue={this.state.name}
                                    onChangeText={props.handleChange('firstname')}
                                    value={props.values.firstname}
                                    onBlur={props.handleBlur('firstname')}
                                />
                                <Text style={style.error_txt}>{props.touched.firstname && props.errors.firstname}</Text>
                                <TextInput 
                                    style={g_style.input}
                                    placeholder={"Email Address"}
                                    defaultValue={this.state.email_address}
                                    keyboardType={'email-address'}
                                    onChangeText={props.handleChange('email')}
                                    value={props.values.email}
                                    onBlur={props.handleBlur('email')}
                                />
                                <Text style={style.error_txt}>{props.touched.email && props.errors.email}</Text>
                                <TextInput 
                                    style={g_style.input}
                                    placeholder={"Password(minimum 6 characters)"}
                                    defaultValue={this.state.password}
                                    secureTextEntry={true}
                                    onChangeText={props.handleChange('password')}
                                    value={props.values.password}
                                    onBlur={props.handleBlur('password')}
                                />
                                <Text style={style.error_txt}>{props.touched.password && props.errors.password}</Text>
                                <TextInput 
                                    style={g_style.input}
                                    placeholder={"Confirm Password"}
                                    defaultValue={this.state.confirm_password}
                                    secureTextEntry={true}
                                    onChangeText={props.handleChange('confirm_password')}
                                    value={props.values.confirm_password}
                                    onBlur={props.handleBlur('confirm_password')}
                                />
                                <Text style={style.error_txt}>{props.touched.confirm_password && props.errors.confirm_password}</Text>

                                <View style={style.checkbox_container}>
                                    <CheckBox
                                        
                                        style={style.checkbox_sign_in}
                                        onClick={()=>{
                                            if (this.state.isChecked) {
                                                this.setState({
                                                    buttonEnabled: true,
                                                })
                                            }else{
                                                this.setState({
                                                    buttonEnabled: false,
                                                })
                                            }
                                            this.setState({
                                                isChecked:!this.state.isChecked
                                            })
                                        }}
                                        isChecked={this.state.isChecked}
                                    />
                                    <Text style={style.checkbox_text}>I Accept the <Text style={style.checkbox_text_modal}>Privacy</Text> and <Text style={style.checkbox_text_modal}>Terms of Use</Text> of this App</Text>
                                </View>
                                    
                                    {/* <Text>I Accept the Privacy and Terms of Use of this App</Text> */}
                                
                                

                                {/* <Button 
                                    style={g_style.button}
                                    title="Create Account" 
                                    disabled={this.state.buttonEnabled}
                                     
                                /> */}
                                <TouchableOpacity 
                                    style={[g_style.button_custom,[this.state.buttonEnabled? g_style.button_disable : g_style.button_enable]]} 
                                    disabled={this.state.buttonEnabled}
                                    onPress={props.handleSubmit}
                                >
                                    <Text
                                        style={g_style.btn_txt}
                                    >
                                        CREATE ACCOUNT
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </TouchableWithoutFeedback>
                    )}
                
                </Formik>
            </View>
            
            </ScrollView>
        </SafeAreaView>
        )
    }
}