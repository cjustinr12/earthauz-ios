import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import StartUp from './Startup'
import Home from './Home'
import SingIn from './SingIn'
import Login from './Login'
import {navigationRef} from './Navroot'

function StartUpScreen() {
    return(
        <StartUp />
    )
}

function HomeScreen({navigation}) {
    return(
        <Home />
    )
}

function SignInScreen({navigation}) {
    return(
        <SingIn/>
    )
}

function LoginScreen({navigation}) {
    return(
        <Login/>
    )
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer ref={navigationRef}>
        <Stack.Navigator 
        initialRouteName="StartUp"
        screenOptions={{
            header: () => null
        }}>
            <Stack.Screen name="StartUp" component={StartUpScreen} />
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="SignIn" component={SignInScreen} />
            <Stack.Screen name="Login" component={LoginScreen} />
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;