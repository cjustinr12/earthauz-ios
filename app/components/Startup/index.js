import React, { Component } from 'react'
import { 
    Text,
    View,
    StyleSheet,
    Animated,
    Image
} from 'react-native';
import style from '../../style/Startup_style/style'
import { withNavigation } from '@react-navigation/native';
import * as RootNavigation from '../Navroot/index.js'


export default class extends Component {
    // state = {
    //     animation: new Animated.Value(0)
    // }
    constructor() {
        super();
        this.state = {
            animation: new Animated.Value(0)
        };
    }

    UNSAFE_componentWillMount(){
        this.state.animation.setValue(0);

        Animated.timing(this.state.animation, {
            toValue: 1,
            duration: 2500,
            useNativeDriver: false
        }).start(({finished}) => {
            if (finished) {
                // this.props.navigation.navigate('Home');
				RootNavigation.navigate('SignIn')
            }
        });
    }
    

    render() {

        const progressInterpolate =
        this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0%", "100%"],
            extrapolate: "clamp"
        })

        const colorInterpolate =
        this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ["#a9a9a9","#a9a9a9"]
        })

        const progressStyle = {
            width: progressInterpolate,
            bottom: 0,
            backgroundColor: colorInterpolate

        }
        
        return (
			<View style={style.startup_container}>
				<Image 
				style={style.startup_logo}
				source={ require('../../assets/img/startup-logo.png')} />
				<View style={style.progressbar}>
					<Animated.View style={[style.progress, progressStyle]} />
				</View>
			</View>
        )
    }
}

// export default withNavigation(StartUp);